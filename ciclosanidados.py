
mensajeEntrada="programa de generacion de tablas de multiplicar".capitalize()
print(mensajeEntrada.center(60,"="))


#opcion=1
opcion= 'S'
#entrar o salir del programa con letras
while opcion !='n' and opcion!='N':
    #entrar o salir con numeros
#while opcion!=0:
    #ingresar valores de tablas
    tabinicio =int(input(chr(27)+"[1;36m"+"ingrese la tabla inicial :"))
    tabfinal= int (input("ingrese la tabla final :"))
#ingresar valores de rangos
    rangeInicio=int(input(chr(27)+"[1;34m"+"ingrese el rango inicial :"))
    rangeFinal=int(input("ingrese el rango final :"))
#controlar valores q ingresa el usuario en las tablas
    while tabfinal<tabinicio:
        print("la tabla de inicio debe ser mayor ")
        tabinicio =int(input(chr(27)+"[1;32m"+"ingrese la tabla inicial :"))
        tabfinal= int (input("ingrese la tabla final :"))
#controlar valores q ingresa el usuario en los rangos
    while rangeFinal<rangeInicio:
        print("el rango de inicio debe ser mayor que el final")
        rangeInicio=int(input(chr(27)+"[1;37m"+"ingrese el rango inicial :"))
        rangeFinal=int(input("ingrese el rango final :"))
#si la tabla inicial es menor se le suma uno hasta que sea igual o mayor
    while tabinicio<tabfinal:
        for i in range(tabinicio,tabfinal+1):
#si la tabla es igual a 4 entonces no la imprimo
            if i==4:
                print("la tabla {0} no la imprimo por que no me da la gana xD".format(i))
                #continue
                #pas es para brincarselo
                pass
#si  yo pongo de uno a 5 le sumo 1 para que me lo imprima hasta el 5 sino llega hasta el 4
            for j in range(rangeInicio,rangeFinal+1):
                if j==5:
                  print("mas de 5 no quiero")
                  #ignorartodo y termoinar
                  break


                resultado=i*j
                #print("multiplicar",i,"*",j,"es igual a :",resultado)
                print(chr(27)+"[1;31m"+"multiplicar %d * %d es igual a :%d"%(i,j,resultado))

            tabinicio=tabinicio+1
#pregunta si desea continuar y salir
        opcion=input("desea ejecutar nuevamente el proceso : \n"
                     "S: continuar \n"
                     "n: para salir\n")
        if (opcion == 'n') or (opcion == 'N'):
            break
else:
    print(chr(27)+"[1;39m"+"gracias por su atencion")
